import React from 'react';
import PropTypes from 'prop-types';
import * as Icon from 'react-feather';

import './input.css';

const Input = (
  {
    className,
    placeholder,
    label,
    type
  },
) => (
  <div class={`form-control ${className}`}>
    {label && (
      <label className="form-control__label">{label}</label>
    )}
    <div className="form-control__content">
      <div className="form-control__faux">
        <input type={type} className="form-control__input form-control__input--input" placeholder={placeholder} />
      </div>
    </div>
  </div>
);

Input.defaultProps = {
  className: '',
  placeholder: '',
  label: '',
  type: ''
};

Input.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string
};

export default Input;
