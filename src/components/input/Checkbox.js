import React from 'react';
import PropTypes from 'prop-types';
import * as Icon from 'react-feather';

import './input.css';

const Checkbox = (
  {
    className,
    label
  },
) => (
  <div class={`form-control form-control--checkbox ${className}`}>
    <div className="form-control__faux">
      <input type="checkbox" className="form-control__input"/>
      <div className="form-control__icon">
        <Icon.Check className="icon icon--sm" />
      </div>
    </div>
    {label && (
      <label className="form-control__label">{label}</label>
    )}
  </div>
);

Checkbox.defaultProps = {
  className: '',
  label: ''
};

Checkbox.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string
};

export default Checkbox;
