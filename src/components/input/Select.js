import React from 'react';
import PropTypes from 'prop-types';
import * as Icon from 'react-feather';

import './input.css';

const Select = (
  {
    className,
    text,
    label
  },
) => (
  <div class={`form-control ${className}`}>
    {label && (
      <label className="form-control__label">{label}</label>
    )}
    <div className="form-control__content">
      <div className="form-control__faux">
        <select className="form-control__input">
          <option selected="true" disabled="disabled">{text}</option>
          <option>Option 2</option>
          <option>Option 3</option>
        </select>
        <Icon.ChevronDown className="form-control__icon icon icon--sm" />
      </div>
    </div>
  </div>
);

Select.defaultProps = {
  className: '',
  text: '',
  label: ''
};

Select.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  label: PropTypes.string
};

export default Select;
