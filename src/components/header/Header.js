import React, {Component} from 'react';
import PropTypes from 'prop-types';
import logo from '../../assets/images/logo.svg';
import Button from '../button/Button';
import * as Icon from 'react-feather';

import './header.css';

class Header extends Component {

  constructor(props) {
    super(props);

    this.state={
      displayMenu: false
    }

    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu(){
    const {displayMenu} = this.state;

    this.setState({
      displayMenu: !displayMenu
    });
  }

  render() {
    const {displayMenu} = this.state;

    return (
      <header className="header">
        <div className="header__container header__container container container--no-margin">
          <div className="header__branding">
            <img className="header__logo" src={logo} />
          </div>
          <nav className={`header__nav${displayMenu ? ' header__nav--visible':'' }`}>
            <Icon.X className="header__nav-close" onClick={this.toggleMenu} />
            <a className="header__nav-item">About</a>
            <a className="header__nav-item">Programs</a>
            <a className="header__nav-item">Locations</a>
            <a className="header__nav-item">Events</a>
            <a className="header__nav-item">Blog</a>
            <a className="header__nav-item">Contact</a>
            <div className="header__nav-item">
              <Button text="Apply" iconName="ArrowRight" />
            </div>
          </nav>
          <Icon.Menu className="header__nav-burger" onClick={this.toggleMenu} />
        </div>
      </header>
    );
  }
}

export default Header;
