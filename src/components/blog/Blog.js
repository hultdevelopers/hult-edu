import React from 'react';
import PropTypes from 'prop-types';
import * as Icon from 'react-feather';

import './blog.css';

const Blog = props => (
  <div className="blog card card--pd-md">
    <div className="blog__category">Careers</div>
    <div className="blog__main">
      <h4 className="blog__heading">
        This is the title of a blog, this is the maximim length...
      </h4>
      <Icon.ArrowRight className="blog__icon icon icon--sm" />
    </div>
  </div>
);

export default Blog;
