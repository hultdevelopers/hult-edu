import React from 'react';
import PropTypes from 'prop-types';
import Button from '../button/Button';
import * as Icon from 'react-feather';
import EventImage from '../../assets/images/event.png';

import './event.css';

const Event = props => (
  <div className="event" style={{backgroundImage: `url(${EventImage})`}}>
    <div className="card m--pd-md">
      <div className="date">23</div>
      <div className="month m--accent">February</div>
      <div className="details">
        <div className="category">Campus Visit</div>
        <h3 className="title">Undergraduate London</h3>
      </div>
    </div>
  </div>
);

export default Event;
