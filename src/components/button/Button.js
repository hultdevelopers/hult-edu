import React from 'react';
import PropTypes from 'prop-types';
import * as Icon from 'react-feather';

import './button.css';

const Button = (
  {
    className,
    iconName,
    text,
    onClick,
  },
) => (
  <button
    className={`btn ${className}`}
    onClick={onClick}
    type="button"
  >
    <span class="btn__text">{text}</span>
    {iconName && React.createElement(Icon[iconName], {
      className: 'btn__icon icon icon--sm',
    })}
  </button>
);

Button.defaultProps = {
  text: '',
  className: '',
  iconName: '',
};

Button.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  iconName: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

export default Button;
