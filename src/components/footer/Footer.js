import React from 'react';
import PropTypes from 'prop-types';
import Button from '../button/Button';
import * as Icon from 'react-feather';

import './footer.css';

const Footer = () => (
<div className="footer">
  <div className="footer__container container">
    <div className="footer__nav grid grid--gutters-sm">
      <div className="footer__nav-item grid__col">Nav Item</div>
      <div className="footer__nav-item grid__col">Nav Item</div>
      <div className="footer__nav-item grid__col">Nav Item</div>
      <div className="footer__nav-item grid__col">Nav Item</div>
      <div className="footer__nav-item grid__col">Nav Item</div>
    </div>
    <Button className="btn--alt" text="Apply" iconName="ArrowRight" />
  </div>
  <div className="footer__container footer__container--bottom container">
    <div className="footer__legal">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      </p>
      <p className="footer__copyright">
        &copy; Hult 2018. All Rights Reserved.
      </p>
    </div>
    <div className="footer__social grid grid--gutters-xsm">
      <div className="grid__col">
        <Icon.Facebook className="footer__icon icon icon--small" />
      </div>
      <div className="grid__col">
        <Icon.Twitter className="footer__icon icon icon--small" />
      </div>
      <div className="grid__col">
        <Icon.Youtube className="footer__icon icon icon--small" />
      </div>
      <div className="grid__col">
        <Icon.Instagram className="footer__icon icon icon--small" />
      </div>
      <div className="grid__col">
        <Icon.Linkedin className="footer__icon icon icon--small" />
      </div>
    </div>
  </div>
</div>
);

export default Footer;
