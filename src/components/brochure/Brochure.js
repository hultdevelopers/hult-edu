import React from 'react';
import PropTypes from 'prop-types';
import * as Icon from 'react-feather';
import Select from '../input/Select';
import Input from '../input/Input';
import Checkbox from '../input/Checkbox';
import Button from '../button/Button';

import './brochure.css';

const Brochure = () => (
  <div className="brochure section">
    <div className="section__container container">
      <h2 className="section__heading heading">Brochure</h2>
    </div>
    <div className="container">
      <div className="brochure__image-container">
        <div className="brochure__card card card--pd-md offset offset--tl-md">
          <h3 className="brochure__heading heading heading--accent">Request a Brochure</h3>
          <p>Lorem ipsum dolor sit amet, dolor adipiscing elit.</p>
          <form className="brochure__form">
            <Select label="Program" />
            <Input
              label="First Name"
              placeholder=""
            />
            <Input
              label="Last Name"
              placeholder=""
            />
            <Input
              label="Email"
              placeholder=""
              type="email"
            />
            <Input
              label="Phone Number"
              placeholder=""
              type="number"
            />
            <Select label="Country of Residence" />
            <Checkbox
              label="Please mail me a printed Brochure"
            />
            <Checkbox
              label="I agree to my personal data being processed in accordance with Hult’s Privacy Policy and for Hult and its affiliates to use my personal data to provide me with further information about its programs."
            />
            <div className="brochure__actions">
              <Button
                text="Submit"
                iconName="Send"
                className="brochure__btn btn--cta"
              />
            </div>
          </form>
        </div>
        <div className="brochure__image"></div>
      </div>
    </div>
  </div>
);

export default Brochure;
