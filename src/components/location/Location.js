import React from 'react';
import PropTypes from 'prop-types';
import Map from '../map/Map';
import * as Icon from 'react-feather';
import Select from '../input/Select';

import './location.css';

const Location = () => (
  <div className="location section">
    <div className="location__container section__container container">
      <h2 className="section__heading location__heading heading">Global Locations</h2>
      <Select
        text="Filter by Program"
        className="location__select"
      />
    </div>
    <div className="location__map-container container">
      <div className="location__map"></div>
      <div className="location__list">
        <div className="campus">
          <span className="campus__abbr">LD</span>
          <div className="campus__details">
            <h3 className="campus__heading">London</h3>
            <Icon.ArrowRight className="campus__icon icon icon--sm" />
          </div>
        </div>
        <div className="campus">
          <span className="campus__abbr">DB</span>
          <div className="campus__details">
            <h3 className="campus__heading">Dubai</h3>
            <Icon.ArrowRight className="campus__icon icon icon--sm" />
          </div>
        </div>
        <div className="campus">
          <span className="campus__abbr">SH</span>
          <div className="campus__details">
            <h3 className="campus__heading">Shanghai</h3>
            <Icon.ArrowRight className="campus__icon icon icon--sm" />
          </div>
        </div>
        <div className="campus">
          <span className="campus__abbr">NY</span>
          <div className="campus__details">
            <h3 className="campus__heading">New York</h3>
            <Icon.ArrowRight className="campus__icon icon icon--sm" />
          </div>
        </div>
        <div className="campus">
          <span className="campus__abbr">BO</span>
          <div className="campus__details">
            <h3 className="campus__heading">Boston</h3>
            <Icon.ArrowRight className="campus__icon icon icon--sm" />
          </div>
        </div>
        <div className="campus">
          <span className="campus__abbr">AS</span>
          <div className="campus__details">
            <h3 className="campus__heading">Ashridge</h3>
            <Icon.ArrowRight className="campus__icon icon icon--sm" />
          </div>
        </div>
        <div className="campus">
          <span className="campus__abbr">SF</span>
          <div className="campus__details">
            <h3 className="campus__heading">San Francisco</h3>
            <Icon.ArrowRight className="campus__icon icon icon--sm" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Location;
