import React from 'react';
import PropTypes from 'prop-types';
import Event from '../event/Event';

import './event-list.css';

const EventList = props => (
  <section className="event-list section">
    <div className="container">
      <h2 className="heading">Upcoming Events</h2>
    </div>
    <div className="container m--m-no-pad">
      <div className="columns m--pd-md">
        <div className="item m--1-of-3 m--md-1-of-1">
          <Event />
        </div>
        <div className="item m--1-of-3 m--md-1-of-1">
          <Event />
        </div>
        <div className="item m--1-of-3 m--md-1-of-1">
          <Event />
        </div>
      </div>
    </div>
  </section>
);

export default EventList;
