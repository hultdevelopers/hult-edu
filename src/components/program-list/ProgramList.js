import React from 'react';
import PropTypes from 'prop-types';
import ProgramListSlider from './ProgramListSlider';
import Program from '../program/Program';

import './program-list.css';

const ProgramList = () => (
  <section className="program-list section">
    <div className="section__container container">
      <h2 className="section__heading heading">Programs</h2>
    </div>
    <div className="container container--sm-no-pad">
      <ProgramListSlider>
        <div className="grid__col grid__col--1-3">
          <Program />
        </div>
        <div className="grid__col grid__col--1-3">
          <Program />
        </div>
        <div className="grid__col grid__col--1-3">
          <Program />
        </div>
        <div className="grid__col grid__col--1-3">
          <Program />
        </div>
        <div className="grid__col grid__col--1-3">
          <Program />
        </div>
      </ProgramListSlider>
    </div>
  </section>
);

export default ProgramList;
