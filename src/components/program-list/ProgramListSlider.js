/* global window, document */
import React, { Component } from 'react';
import Slider from 'react-slick';

import './program-list.css';
import '../../../node_modules/slick-carousel/slick/slick.css';
import '../../../node_modules/slick-carousel/slick/slick-theme.css';

class ProgramListSlider extends Component {
  constructor(props) {
    super(props);

    this.onWindowResize = this.onWindowResize.bind(this);

    this.state = {
      slickIt: false,
    };
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      window.onresize = this.onWindowResize;
    }
  }

  onWindowResize() {
    const width = document.documentElement.clientWidth
      || document.body.clientWidth;
    const { slickIt } = this.state;
    const showSlider = width <= 768;

    console.log(width);
    console.log(window.innerWidth);
    console.log(document.documentElement.clientWidth);
    console.log(document.body.clientWidth);

    if (showSlider !== slickIt) {
      this.setState({ slickIt: showSlider });
    }
  }

  render() {
    const { children } = this.props;
    const { slickIt } = this.state;

    if (slickIt) {
      return (
        <Slider>
          {children}
        </Slider>
      );
    }

    return (
      <div className="grid">
        {children}
      </div>
    );
  }
}

export default ProgramListSlider;
