import React from 'react';
import PropTypes from 'prop-types';
import Button from '../button/Button';
import * as Icon from 'react-feather';
import Slider from "react-slick";

import './program.css';

const Program = props => (
  <div className="program card card--pd-md">
    <div className="program__card-content">
      <h3 className="program__heading heading heading--accent">Undergraduate</h3>
      <p>
        Full-time programs for recent graduates
        or candidates with less than 3 years’
        work experience.
      </p>
      <Button className="program__btn btn--ghost" text="Learn More" iconName="ArrowRight" />
    </div>
  </div>
);

export default Program;
