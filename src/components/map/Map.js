import React from 'react';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';

import './map.css';


class Map extends React.Component {
  static defaultProps = {
    center: {lat: 0, lng: 0},
    zoom: 1
  };

  render() {
    return (
      <div className="map"></div>
    );
  }
}

export default Map;
