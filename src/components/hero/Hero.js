import React from 'react';
import PropTypes from 'prop-types';
import HeroImage from '../../assets/images/hero.jpg';
import Blob from '../../assets/images/blobs-2.png';
import Button from '../button/Button';

import './hero.css';

const Hero = () => (

  <div className="hero">
    <div className="hero__container container m--no-pad">

      <div className="hero__image-container" style={{backgroundImage: `url(${HeroImage})`}}>

        <div className="hero__card card card--pd-md offset offset--bl-md">
          <div className="hero__card-content">
            <h1 className="hero__heading heading--accent">Go beyond<br />business</h1>
            <p>
              At Hult, we believe a business school should give you so
              much more than a degree. That’s why here, you’ll learn
              about the world, the future and yourself, so you can
              make an impact that lasts.
            </p>
            <Button className="hero__btn" text="Watch" iconName="Play" />
          </div>
        </div>

        <img className="hero__shapes" src={Blob} />

      </div>

    </div>
  </div>

);

export default Hero;
