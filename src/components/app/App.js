import React, { Component } from 'react';
import Header from '../header/Header';
import Hero from '../hero/Hero';
import ProgramList from '../program-list/ProgramList';
import BlogList from '../blog-list/BlogList';
import Location from '../location/Location';
import Brochure from '../brochure/Brochure';
import Footer from '../footer/Footer';

import './app.css';

class App extends Component {
  render() {
    return (
      <div className="app">

        <Header />

        <Hero />

        <ProgramList />

        <BlogList />

        <Location />

        <Brochure />

        <Footer />

      </div>
    );
  }
}

export default App;
