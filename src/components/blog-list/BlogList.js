import React from 'react';
import PropTypes from 'prop-types';
import Blog from '../blog/Blog';
import BlogImage from '../../assets/images/blog.png';

import './blog-list.css';

const BlogList = () => (
  <div className="blog-list section">
    <div className="section__container container">
      <h2 className="section__heading heading">Blog</h2>
    </div>
    <div className="blog-list__container container">
      <div className="blog-list__image-container" style={{backgroundImage: `url(${BlogImage})`}}>
        <div className="blog-list__posts offset offset--tl-md">
          <Blog />
          <Blog />
          <Blog />
        </div>
      </div>
    </div>
  </div>
);

export default BlogList;
