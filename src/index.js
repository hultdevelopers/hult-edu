import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app/App';
import registerServiceWorker from './registerServiceWorker';

import './assets/scss/style.css';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
